#include <OctoWS2811.h>

#define LEDS_PER_STRIP 50
#define CONTROL 10
#define MAX_CNTRL_VAL 127
#define TRESH_YELLO 35
#define TRESH_RED 45

#define BLACK 0x000000
#define RED 0xFF0000
#define GREEN 0x00FF00
#define YELLOW 0xFFFF00
#define ORANGE 0xFF3F00

#define PEAK_FREEZE_TIME 20
#define PEAK_FALLOFF_ACCEL .01
#define FALLOFF_SPEED 4.0

IntervalTimer _timer;

DMAMEM int _displayMemory[LEDS_PER_STRIP * 6];
int _drawingMemory[LEDS_PER_STRIP * 6];

const int _config = WS2811_RGB | WS2811_800kHz;
volatile float _peakPos = 0;
volatile float _peakSpeed = 0;
volatile int _peakFreezeCounter = 0;

OctoWS2811 _leds(LEDS_PER_STRIP, _displayMemory, _drawingMemory, _config);

volatile float _cntrlVal = 0;
float _valCur;

void setup() {
  _leds.begin();
  _leds.show();
  usbMIDI.setHandleControlChange(OnControlChange);
  _timer.begin(draw, 10000);  // 100 Hz
}

void loop() {
  usbMIDI.read();
}

unsigned long setColBrightness(unsigned long color, float brightness) {
 int nBright = (int) (brightness * 255.5);
 
 int r = color >> 16;
 int g = (color >> 8) & 0xFF;
 int b = color & 0xFF;
 
 r = (r * nBright) >> 8;
 g = (g * nBright) >> 8;
 b = (b * nBright) >> 8; 
 
 return (r << 16) | (g << 8) | b;
}

void draw() {
  if (_cntrlVal > _valCur) _valCur = _cntrlVal;
  int val = (int) ((_valCur * LEDS_PER_STRIP) / MAX_CNTRL_VAL + .5);
  if (val > _peakPos) {
    _peakPos = val;
    _peakSpeed = 0;
    _peakFreezeCounter = PEAK_FREEZE_TIME;
  }
  for (int i = 1; i <= LEDS_PER_STRIP; ++i) {
    if (i > val) _leds.setPixel(i - 1, BLACK);
    else if (i <= TRESH_YELLO) _leds.setPixel(i - 1, GREEN);
    else if (i <= TRESH_RED) _leds.setPixel(i - 1, ORANGE);
    else _leds.setPixel(i - 1, RED);
  }

  // peak pixel
  // int nPeakPos = (int) (_peakPos + .5);
  if (_peakPos > val) {
    int nPeakPos = (int) _peakPos;
    float f = _peakPos - nPeakPos;
    
    unsigned int col;
    if (_peakPos <= TRESH_YELLO) col = GREEN;
    else if (_peakPos <= TRESH_RED) col = ORANGE;
    else col = RED;
    
    if (nPeakPos < LEDS_PER_STRIP) _leds.setPixel(nPeakPos, setColBrightness(col, f));
    if (nPeakPos > val) _leds.setPixel(nPeakPos - 1, setColBrightness(col, 1.0 - f));

    // Calculate new peak position
    if (_peakFreezeCounter) --_peakFreezeCounter;
    else {
      _peakPos = _peakPos - _peakSpeed;    
      _peakSpeed += PEAK_FALLOFF_ACCEL;  
    }
  }  

  _leds.show();

  // Falloff
  if (_valCur > _cntrlVal) _valCur = max(_valCur - FALLOFF_SPEED, _cntrlVal);
}

void OnControlChange(byte channel, byte control, byte value) {
  switch (control) {
  case CONTROL:
    _cntrlVal = value;
    break; 
  }
}

